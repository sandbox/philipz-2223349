<?php

/**
 * @file
 * Implements Dotpay URLC transaction notifications in Drupal Commerce.
 */

/**
 * Processes an incoming URLC.
 *
 * Print OK if URLC was successfully processed and exit in case.
 */
function commerce_dotpay_urlc($debug_urlc = array()) {
  // Retrieve the URLC from $_POST.
  // Note that Drupal has already run stripslashes() on the contents of the
  // $_POST array at this point, so we don't need to worry about them.
  $urlc = $_POST;

  // Exit now if the $_POST was empty.
  if (empty($urlc)) {
    watchdog('commerce_dotpay', 'URLC accessed with no POST data submitted.', array(), WATCHDOG_WARNING);
    return FALSE;
  }

  // Load the order based on the URLC's control number.
  $urlc['order_id'] = !empty($urlc['control']) ? $urlc['control'] : 0;
  if (!empty($urlc['order_id'])) {
    $order = commerce_order_load($urlc['order_id']);
    $payment_method_instance_id = $order->data['payment_method'];
    $payment_method = commerce_payment_method_instance_load($payment_method_instance_id);
  }
  else {
    $order = FALSE;
  }

  // Validate the URLC
  // Only exit if the function explicitly returns FALSE.
  if (commerce_dotpay_urlc_validate($order, $payment_method, $urlc) === FALSE) {
    drupal_exit();
  }

  // Process the URLC.
  if (commerce_dotpay_urlc_process($order, $payment_method, $urlc) !== FALSE) {
    print 'OK';
  }
  drupal_exit();
}

/**
 * Validate an URLC based on receiver md5 hash and price.
 *
 * @return bool
 *   Do values submitted by URLC match transaction's values?
 */
function commerce_dotpay_urlc_validate($order, $payment_method, $urlc) {
  if (empty($urlc['control'])) {
    return FALSE;
  }
  // Validate URLC md5 hash.
  $md5 = array(
    $payment_method['settings']['commerce_dotpay_pin'],
    $payment_method['settings']['commerce_dotpay_id'],
    $urlc['control'],
    $urlc['t_id'],
    $urlc['amount'],
    $urlc['email'],
    $urlc['service'],
    $urlc['code'],
    $urlc['username'],
    $urlc['password'],
    $urlc['t_status'],
  );
  if (md5(implode(':', $md5)) != $urlc['md5']) {
    watchdog('commerce_dotpay', 'URLC md5 invalid.', array(), WATCHDOG_WARNING);
    return FALSE;
  }
  // Validate transaction amount.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $amount = $wrapper->commerce_order_total->amount->value();
  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $total = commerce_currency_amount_to_decimal($amount, $currency_code);
  if ($total != $urlc['amount']) {
    watchdog('commerce_dotpay', 'Wrong amount: !order_total <> @amount', array(
      '!order_total' => $total,
      '@amount' => $urlc['amount'],
      ), WATCHDOG_WARNING);
    return FALSE;
  }
  return TRUE;
}

/**
 * Payment method callback: process an URLC once it's been validated.
 */
function commerce_dotpay_urlc_process($order, $payment_method, &$urlc) {
  // Load the prior URLC's transaction and update that with the capture values.
  $transaction = commerce_payment_transaction_load($urlc['transaction_id']);
  if (empty($transaction)) {
    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new('dotpay', $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];
  }

  $transaction->remote_id = $urlc['transaction_id'];
  $original_amount = explode(" ", $urlc['orginal_amount'], 2);
  if (count($original_amount) > 1) {
    $transaction->amount = commerce_currency_decimal_to_amount($original_amount[0], $original_amount[1]);;
    $transaction->currency_code = $original_amount[1];
  }
  $transaction->payload[REQUEST_TIME . '-urlc'] = $urlc;

  // Set the transaction's statuses based on the URLC's t_status.
  $transaction->remote_status = $urlc['t_status'];

  switch ($urlc['t_status']) {
    // New.
    case '1':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('The payment is being processed.');
      break;

    // Payment received.
    case '2':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('The payment has completed.');
      // User is not returned to the store, but paid for the order
      // so we close the transaction.
      if ($order->status == 'checkout_payment') {
        commerce_checkout_complete($order);
      }
      break;

    // Rejected.
    case '3':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('The authorization was voided.');
      break;

    // Cancelled.
    case '4':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t("The payment has failed. This happens only if the payment was made from your customer’s bank account.");
      break;

    // Refunded.
    case '5':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Refund for transaction.');
      break;
  }

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);
}
