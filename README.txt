INTRODUCTION
------------
This module adds new payment method for Drupal Commerce allowing users
to complete checkout using Polish offsite payment gateway Dotpay (dotpay.pl).
* For a full description of the module, visit the project page:
  https://drupal.org/sandbox/philipz/2223349
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2223349

REQUIREMENTS
------------
This module requires the following modules:
* Commerce (https://drupal.org/project/commerce
)

INSTALLATION
——————
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
 * After enabling module there will be new payment method
   available under admin/commerce/config/payment-methods.
 * Enable Dotpay payment method and edit its settings.
 * To configure and start using Dotpay you will need an account
   created at http://www.dotpay.pl
 * After getting your account you will have an id assigned.
 * Next choose and set pin string for confirmations sent to URLC.
 * Last thing to set is allowing remote services to send URLC parameter.
 * Now go back to payment method settings and fill both id and pin values.
 * Online transfer checkbox tells the gateway to present user only with payment
   options that will be processed right away (some payment options might not
   work during weekend).

MAINTAINERS
-----------
Current maintainers:
* Paweł Philipczyk (philipz) - https://drupal.org/user/750632
